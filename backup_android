#!/bin/bash
# Backup an Android phone over adb (much faster than MTP):
# - TitaniumBackup directory
# - WhatsApp directory
# - DCIM pictures directory
# - manually exported contacts VCF file
#
# Prerequisites:
# 1. USB debugging is enabled so that adb works
# 2. A contact VCF export dated today is available on the phone using the
# format expected by the script
#
# If 'adb devices' does not return anything, check:
# - debugging is enabled and connection approved
# - phone is unlocked
#
# TODOs:
# - check the phone is present
# - automate contacts backups/exports
# - skip contact xfer if file is not present
# - check all apps have correct backsup using TitaniumBackup and don't need a
# dedicated backup dir
# - provide restore steps

set -o errexit

stamp="$(date +'%Y-%m-%d')"

adb="$HOME/Documents/Dev/Android/platform-tools_r29.0.5-linux/adb"
backup_root="$HOME/Documents/Backups/Android"
ti_dir="TitaniumBackup"
wa_dir="WhatsApp"
pics_dir="DCIM"

backup_path="$backup_root/$stamp"
phone_root="/storage/self/primary"

$adb devices

if [[ -d $backup_path ]]; then
    echo "error: backup path $backup_path already exists! Aborting."
    exit 1
fi
mkdir $backup_path
for dir in "$ti_dir" "$wa_dir" "$pics_dir"; do
    $adb pull "$phone_root/$dir" "$backup_path"
done
$adb pull "$phone_root/ContactsBackups/contacts-$stamp.vcf" $backup_path
